'use strict';

// Modules
const webpack = require('webpack');
const path = require('path');
// Webpack plugins
const UglifyJsPlugin = require('webpack-uglify-js-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BowerWebpackPlugin = require("bower-webpack-plugin");
// Paths
const SRC = path.join(__dirname, 'src');
const VIEWS = path.join(SRC, 'views');
const DIST = path.join(__dirname, 'dist');
// Configuration
var app_config = require(SRC + '/app/config');

const config = {
    cache: true,
    debug: true,
    name: 'browser',
    devtool: 'source-map',
    context: SRC,
    entry: {
        'app': './app/bootstrap.js',
        'home': './views/sections/home/main.js',
        'the-institute': './views/sections/the-institute/main.js'
    },
    output: {
        path: DIST,
        filename: "assets/js/[name].bundle.js?[hash]"
    },
    module: {
        loaders: [
            { test: /\.handlebars$/, loader: "handlebars-loader?helperDirs[]=" + VIEWS + "/helpers", exclude: /(node_modules)/ },
            { test: /\.html$/, loader: 'html-loader', exclude: /(node_modules)/ },
            { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader?sourceMap!less-loader?sourceMap")
            },
            { test: /\.json$/, exclude: /(node_modules)/, loader: 'json' },
            {
                test: /\.png$/,
                loader: "url-loader?limit=10000&mimetype=image/png&name=assets/img/[name].[ext]"
            },
            {
                test: /\.(jpe?g|gif|ico)$/,
                loader: 'file-loader?name=assets/img/[name].[ext]'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff&name=[name].[ext]'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader?limit=10000&mimetype=application/octet-stream&name=[name].[ext]'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader?name=[name].[ext]'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url-loader?limit=10000&mimetype=image/svg+xml&name=[name].[ext]"
            }
        ]
    },
    resolve: {
        modulesDirectories: ["node_modules"]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false }
        }),
        new ExtractTextPlugin("./assets/css/[name].css?[hash]"),
        new HtmlWebpackPlugin({
            data: app_config,
            inject: true,
            filename: 'index.html',
            template: 'views/sections/home/index.handlebars',
            chunks: ['app', 'home'],
            chunksSortMode: 'none'
        }),
        new HtmlWebpackPlugin({
            data: app_config,
            inject: true,
            filename: 'the-institute.html',
            template: 'views/sections/the-institute/index.handlebars',
            chunks: ['app', 'the-institute'],
            chunksSortMode: 'none'
        })
    ],
    devServer: {
        historyApiFallback: true
    }
};
module.exports = config;