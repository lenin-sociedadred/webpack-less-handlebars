'use strict';

const path = require('path');
const fs = require('fs');

var configuration = {};
// Load and merge all configuration files
configuration['pages'] = JSON.parse(fs.readFileSync(path.join(__dirname, 'pages.json'), 'utf8'));
configuration['metatags'] = JSON.parse(fs.readFileSync(path.join(__dirname, 'metatags.json'), 'utf8'));
configuration['navbar'] = JSON.parse(fs.readFileSync(path.join(__dirname, 'navbar.json'), 'utf8'));
configuration['footer'] = JSON.parse(fs.readFileSync(path.join(__dirname, 'footer.json'), 'utf8'));

module.exports = configuration;