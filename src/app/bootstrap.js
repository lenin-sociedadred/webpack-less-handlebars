'use strict';

// From NPM
require('jquery.mmenu');

// Load LESS Files
require('./styles')();

(function($) {
    // YAMM
    console.log('YAMM');
    $(document).on('click', '.yamm .dropdown-menu', function(event) {
        event.stopPropagation();
    });
    console.log('MMenu');
    // MMenu
    $("#sitemap").mmenu({
        "slidingSubmenus": true,
        "keyboardNavigation": true,
        "screenReader": true,
        "navbar": {
            "add": true,
            "title": 'Mapa de sitio'
        },
        "extensions": [
            "pagedim-black"
        ],
        "offCanvas": {
            "pageSelector": "#my-wrapper",
            "position": "left",
            "zposition": "front"
        }
    });
})(jQuery);