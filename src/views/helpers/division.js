/**
 * Simple division helper
 * {{{division 12 ../sub.length}}}
 */
module.exports = function (a, b, options) {
    if ( b > 0 ) {
        return a / b;
    } else {
        return 0;
    }
};