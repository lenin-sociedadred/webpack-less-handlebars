/**
 * Handlebars.js if block helper ==
 * http://stackoverflow.com/a/15095019/1006079
*/
module.exports = function (a, b, options) {
    if(a === b) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
};