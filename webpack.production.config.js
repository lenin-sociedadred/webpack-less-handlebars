'use strict';

// Modules
const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('webpack-uglify-js-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const _ = require('underscore');
// Paths
const SRC = path.join(__dirname, 'src');
const DIST = path.join(__dirname, 'dist');
// Configuration
var app_config = require(SRC + '/app/config');
console.log( app_config );

const config = {
	cache: false,
	debug: false,
	name: 'browser',
	devtool: 'eval',
	context: SRC,
	entry: {
		'app': './app/bootstrap.js',
		'home': './views/sections/home/main.js',
		'the-institute': './views/sections/the-institute/main.js'
    },
	output: {
        path: DIST,
		filename: "assets/js/[name].bundle.js?[hash]"
	},
	module: {
		loaders: [
			{ test: /\.handlebars$/, loader: "handlebars-loader", exclude: /(node_modules)/ },
			{ test: /\.html$/, loader: 'html-loader', exclude: /node_modules/ },
			{ test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
			{ test: /\.less$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader?sourceMap!less-loader?sourceMap") },
			{ test: /\.json/, loader: 'json-loader'},
			{ test: /\.png$/,  loader: "url?limit=10000&mimetype=image/png" },
			{ test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
			{ test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
			{ test: /\.eot$/,  loader: "file" },
			{ test: /\.svg$/,  loader: "url?limit=10000&mimetype=image/svg+xml" },
		],
	},
	resolve: {
        modulesDirectories: ["node_modules"]
    },
	plugins: [
		new webpack.optimize.CommonsChunkPlugin('common.js'),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin(),
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(true),
		new webpack.optimize.UglifyJsPlugin({
            compress: {warnings: false}
        }),
		new ExtractTextPlugin("./assets/css/[name].css?[hash]"),
		new HtmlWebpackPlugin({
			data: app_config,
			inject: true,
			filename: 'index.html',
			template: 'views/sections/home/index.handlebars',
			chunks: ['app', 'home'],
			chunksSortMode: 'none'
		}),
		new HtmlWebpackPlugin({
			data: app_config,
			inject: true,
			filename: 'the-institute.html',
			template: 'views/sections/the-institute/index.handlebars',
			chunks: ['app', 'the-institute'],
			chunksSortMode: 'none'
		})
	],
	devServer: {
		historyApiFallback: true
	}
};
module.exports = config;